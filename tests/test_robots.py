import unittest
import urllib.robotparser

class TestRobots(unittest.TestCase):
    
    def test_can_fetch(self):
        url = "https://kabuoji3.com/"
        rp = urllib.robotparser.RobotFileParser()
        rp.set_url(url + "robots.txt")
        rp.read()

        useragent = "*"
        retult = rp.can_fetch(useragent, url)

        self.assertTrue(retult)